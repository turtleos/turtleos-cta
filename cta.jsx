import * as uuid from 'uuid';

import { pm } from '../turtleos-pm/pm';

function cta() {
    this.notifications={};
    this.popups={};

    const generateUUID = (obj) => {
        let id = uuid.v4();
        if(obj[id]) return generateUUID(obj);
        return id;
    }

    const update = ([key, value]) => {
        pm.dispatch({
            type: 'cta:update',
            payload: {
                key: key,
                value: value
            }
        })
    }

    return {

    addNotification: ({icon, title, content, onClick}) => {
        let id = generateUUID(this.notifications);
        this.notifications[id]={
            icon: icon,
            title: title,
            content: content,
            onClick: onClick || (()=>{}),
            id: id
        };
        update(['notifications', this.notifications]);
        return id;
    },

    removeNotification: (id) => {
        if(this.notifications[id]) delete this.notifications[id];
        update(['notifications', this.notifications]);
    },


    addPopup: ({component}) => {
        let id = generateUUID(this.popups);
        this.popups[id]={
            id: id,
            component: component
        };
        update(['popups', this.popups]);
        return id;
    },

    removePopup: (id) => {
        if(this.popups[id]) delete this.popups[id];
        update(['popups', this.popups]);
    },

        popups: this.popups,
        notifications: this.notifications
    }
}

export {
    cta
}
